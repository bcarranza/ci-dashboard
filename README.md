# CI adoption report

Report on the adoption of specific GitLab CI features for a number of projects or groups.

## Features

- Parse the CI files of a number of projects for configurable CI keyword combinations (like `artifacts:reports:sast`)
- Uses the CI Lint API to resolve `includes` and search the complete, merged `.gitlab-ci.yml` file
- Publish a report via GitLab pages

## Usage

### Forking and running this project on GitLab.com

If you use GitLab SaaS, you can simply:
* fork this project
* edit `config.yml` to define groups, projects and adoption criteria as described below
* add your access token as a CI/CD Variable
* run a pipeline

### Installing on locally or on self-hosted GitLab

`pip3 install -r requirements.txt`

### Running the report

`python3 create_adoption_dashboard.py $GIT_TOKEN $CONFIG_YML --gitlab $GITLAB_URL`

* $GIT_TOKEN: An access token with API and Repository permissions for the configured groups and projects
* $CONFIG_YML: Path to the `config.yml` file containing the configuration
* $GITLAB_URL: Optional URL of the GitLab instance. Defaults to `https://gitlab.com`

### Accessing the dashboard

Because the `index.html` file loads the report file from the disk, you will need a webserver to view the files locally.

Using GitLab pages, just browse to Project Settings -> Pages and find the pages URL of the project,

## Configuration

### Define groups, projects and adoption criteria

- edit config.yml
  - specify groups or projects using their ID (required)
  - If groups are used, all projects in the group are retrieved instead of individual projects.
  - specify adoption criteria: (optional)
    - Each criterium needs a unique key and values `name` and `path`
    - `path` refers to a combination of nested properties in `.gitlab-ci.yml` files. For example [`artifacts:reports:sast`](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html), specifying the SAST scanning report.
    - If the path is found in the `yml` file, output for the criterium will be `True` for the project, else `False`.

### Set up the repository

- delete [data/adoption_data.json](data/adoption_data.json). The file included in this project is used for demo purposes. This file will be used to store incremental adoption data. So for each pipeline run, a new data series will be added to the file.
- it may make sense to track this file via git lfs after the first push:
  - `git lfs install`
  - `git lfs track data/adoption_data.json`

## Use cases

* Track the adoption of SAST or secret detection in a group:

```yml
adoption_criteria:
  secret_detection:
    name: "Secret Detection"
    path: "artifacts:reports:secret_detection"
  sast:
    name: "SAST"
    path: "artifacts:reports:sast"
```

* Track that [`environment:deployment_tier`](https://docs.gitlab.com/ee/ci/environments/#deployment-tier-of-environments) is specified. Useful if you want to use [group-level protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#group-level-protected-environments).

```yml
adoption_criteria:
  environment_type:
    name: "Environment type"
    path: "environment:deployment_tier"
```

## Limitations

* [Compliance pipelines](https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-pipeline-configuration): if you are using compliance pipelines, the jobs they enforce are not added to the `.gitlab-ci.yml` files of the individual projects. They are also not resolved by the `CI lint API`. Thus, it is not possible to track their adoption using this dashboard.

## DISCLAIMER

This script is provided **for educational purposes only**. It is not supported by GitLab. You can create an issue or contribute via MR if you encounter any problems.
